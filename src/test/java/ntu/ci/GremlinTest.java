package ntu.ci;

import com.orientechnologies.orient.core.db.document.ODatabaseDocumentTx;
import com.orientechnologies.orient.core.record.impl.ODocument;
import com.orientechnologies.orient.core.sql.query.OSQLSynchQuery;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;
import com.tinkerpop.blueprints.impls.orient.OrientGraph;
import com.tinkerpop.blueprints.impls.orient.OrientGraphNoTx;
import com.tinkerpop.gremlin.java.GremlinPipeline;
import com.tinkerpop.pipes.PipeFunction;
import ntu.ci.database.OrientFactory;
import ntu.ci.model.Node;
import org.javatuples.Quartet;
import org.javatuples.Triplet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * Created by dgee on 20/2/17.
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
@DirtiesContext
public class GremlinTest extends AbstractTestNGSpringContextTests {

    private final Logger logger = LoggerFactory.getLogger(GraphappApplicationTests.class);

    @Autowired
    private OrientFactory orientFactory;

    @BeforeClass
    public void init() {
        try {
            final int delay = 2;
            logger.info("Giving {} seconds to wait for initialization.", delay);
            TimeUnit.SECONDS.sleep(delay);
            logger.info("Starting test executions ..");
        }
        catch(InterruptedException ex) {}
    }

    @Test
    public void test() {
        ODatabaseDocumentTx database = orientFactory.getFactory().getDatabase();

        final String osql = new StringBuilder()
                .append("select count, inV().lemmatization as lemma1, inV()['@rid'] as id1, outV().lemmatization as lemma2, outV()['@rid'] as id2, @class from ")
                .append("(Select expand(bothE()) from #24:2559) ")
                .append("where count > 1 order by count desc")
                .toString();

        logger.debug("OSQL test: {}.", osql);

        List<ODocument> results = database
                .command(new OSQLSynchQuery<ODocument>(osql)).execute();

        for(ODocument d: results) {
            logger.debug("JSON is: {}.", d.toJSON());
        }
    }

    @AfterTest
    public void destroy() {
        orientFactory.shutdown();
    }
}
