package ntu.ci;

import com.orientechnologies.orient.core.db.document.ODatabaseDocumentTx;
import com.orientechnologies.orient.core.record.impl.ODocument;
import com.orientechnologies.orient.core.sql.query.OSQLSynchQuery;
import com.tinkerpop.blueprints.Vertex;
import com.tinkerpop.blueprints.impls.orient.OrientGraph;
import com.tinkerpop.blueprints.impls.orient.OrientGraphNoTx;
import com.tinkerpop.blueprints.impls.orient.OrientVertex;
import com.tinkerpop.blueprints.util.io.graphson.GraphSONMode;
import com.tinkerpop.blueprints.util.io.graphson.GraphSONWriter;
import com.tinkerpop.gremlin.java.GremlinPipeline;
import ntu.ci.database.OrientFactory;
import ntu.ci.model.Node;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
@DirtiesContext
public class GraphappApplicationTests extends AbstractTestNGSpringContextTests {

	// See Graph API @ http://orientdb.com/docs/last/Graph-Database-Tinkerpop.html

	// Some test codes on OrientDB github:
	// https://github.com/orientechnologies/orientdb/blob/master/tests/src/test/java/com/orientechnologies/orient/test/database/auto/TraverseTest.java

	private final Logger logger = LoggerFactory.getLogger(GraphappApplicationTests.class);

	@Autowired
	private OrientFactory orientFactory;

	// Test Node
	private ODocument testNode;

	@BeforeClass
	public void createTestDatabase() {
		try {
			final int delay = 2;
			logger.info("Giving {} seconds to wait for initialization.", delay);
			TimeUnit.SECONDS.sleep(delay);
			logger.info("Starting test executions ..");
		}
		catch(InterruptedException ex) {}
	}

	private void createGraphProperties() {
		OrientGraph graph = orientFactory.getFactory().getTx();
		graph.setUseLightweightEdges(false); // so that we can set label for the edge
		graph.createVertexType("Node");
		graph.createVertexType("Concept");
		graph.createEdgeType("connectsTo");
		graph.createEdgeType("has");
		graph.commit();
		graph.shutdown();
	}

	// Example on how to add vertex and create edge among them
	@Test
	public void example_addVertex() {
		OrientGraph graph = orientFactory.getFactory().getTx();

		try {
			// Using common interface Blueprints [Vertex] class
			Vertex node1 = graph.addVertex("class:Node", "name", "Node 1", "published", 1988);
			node1.setProperty("group", "Group A");
			logger.debug("Create Node 1 vertex id: {}.", node1.getId());

			// Using OrientDB API [Vertex] class
			OrientVertex node2 = graph.addVertex("class:Node");

			// Another way to set properties
			Map<String, Object> props = new HashMap<String, Object>(0);
			props.put("name", "Node 2");
			props.put("group", "Group G");
			props.put("published", 1991);
			node2.setProperties(props);
			logger.debug("Create Node 2 vertex rid: {}.", node2.getRecord().getIdentity().toString());

			graph.addEdge(null, node1, node2, "connectsTo");

			// Using OrientDB Document API
			testNode = graph.addVertex("class:Concept", "subject", "testing").getRecord();

			// Another way to add edge using the Document object
			graph.addEdge(null, node1, graph.getVertex(testNode), "has");

			graph.commit();
		}
		catch(Exception ex) {
			logger.error("Rolling back graph db.", ex);
			graph.rollback();
		}
		finally {
			graph.shutdown();
		}
	}

	// example to select all nodes on the graph
	@Test(dependsOnMethods = { "example_addVertex" })
	public void example_selectAllNodes() {
		OrientGraph graph = orientFactory.getFactory().getTx();

		try {
			for(final Vertex v: graph.getVertices()) {
				final Node node = Node.build(v);
				logger.debug("Found node with id {} with properties [{}].", node.getId(), node.getProperties());
			}
		}
		finally {
			graph.shutdown();
		}
	}

	// example to select one node based on query on the property "name"
	@Test(dependsOnMethods = { "example_addVertex" })
	public void example_selectOneNode() {
		OrientGraph graph = orientFactory.getFactory().getTx();

		try {
			Iterable<Vertex> vertices = graph.query().has("name", "Node 2").vertices();
			vertices.forEach(v -> {
				final Node node = Node.build(v);
				logger.debug("Found [Node 2] vertex with id: {}.", node.getId());
			});
		}
		finally {
			graph.shutdown();
		}
	}

	// example to print the entire graph in GraphSON (json format)
	@Test(dependsOnMethods = { "example_addVertex" })
	public void example_printGraphInJsonWithOrientAPI() {
		final ByteArrayOutputStream output = new ByteArrayOutputStream();
		OrientGraphNoTx graphNoTx = orientFactory.getFactory().getNoTx();

		try {
			new GraphSONWriter(graphNoTx).outputGraph(output, null, null, GraphSONMode.NORMAL);
			final String json = output.toString("UTF-8");

			logger.debug("Graph JSON: {}", json);

			// if we want specific properties only
			/*
			final Set<String> vertexProps = new HashSet<String>(0);
			vertexProps.add("name");

			final Set<String> edgeProps = new HashSet<String>(0);
			edgeProps.add("has");

			new GraphSONWriter(graphNoTx).outputGraph(output, vertexProps, edgeProps, GraphSONMode.NORMAL);
			*/

		}
		catch(IOException ex) {
			logger.error("Failed to write graph as JSON.");
		}
		finally {
			graphNoTx.shutdown();
		}
	}

	// example to traverse the graph using OrientDB syntax
	@Test(dependsOnMethods = { "example_addVertex" })
	public void example_traverseByOrientSQL() {
		ODatabaseDocumentTx database = orientFactory.getFactory().getDatabase();

		final String osql = new StringBuilder()
				.append("select from (traverse in() from ")
				.append(testNode.getIdentity())
				.append(") where @class = 'Node'")
				.toString();

		logger.debug("OSQL test: {}.", osql);

		List<ODocument> results = database
				.command(new OSQLSynchQuery<ODocument>(osql)).execute();

		final String outputFormat = "rid,version,class,indent:1";

		for(ODocument d: results) {
			logger.debug("Traverse Result Found! Class: {}, Name: {}", d.getClassName(), d.field("name"));
			logger.debug("JSON is: {}.", d.toJSON(outputFormat));
		}

		database.close();
	}

	// example to traverse the graph using Gremlin API, this is the same traverse path as per above.
	@Test(dependsOnMethods = { "example_addVertex" })
	public void example_traverseByGremlinPipe() {
		OrientGraphNoTx graph = orientFactory.getFactory().getNoTx();

		try {
			GremlinPipeline pipe = new GremlinPipeline();
			Vertex root = graph.getVertex(testNode);

			List<Vertex> list = pipe.start(root).inE("has").outV().toList();
			list.forEach(v -> {
				final Node node = Node.build(v);
				logger.debug("Traverse Gremlin Found! ID: {}, Properties: {}.", node.getId(), node.getProperties());
			});
		}
		finally {
			graph.shutdown();
		}
	}

	@AfterClass
	public void dropTestDatabase() {
		orientFactory.drop();
	}

}
