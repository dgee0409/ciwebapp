package ntu.ci.controller;

import ntu.ci.service.ImportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

/**
 * Created by dgee on 9/1/17.
 */

@Controller
public class MainController {

    private final String homepageUrl = "static/index.html";

    @Autowired private ImportService importService;

    @RequestMapping(value="/", method = RequestMethod.GET)
    public String homepage() {
        return homepageUrl;
    }

    @RequestMapping(value="/import", method = RequestMethod.POST)
    public @ResponseBody String importFiles(@RequestParam("files") MultipartFile [] files) {
        int count = 0;
        for(MultipartFile file: files) {
            boolean success = importService.processData(file);
            if(success) count++;
        }
        final String message = count + "/" + files.length + " files imported successfully.";
        return message;
    }

    @RequestMapping(value="/dictionary", method = RequestMethod.POST)
    public @ResponseBody String importDictionary(@RequestParam("files") MultipartFile [] files) {
        int count = 0;
        for(MultipartFile file: files) {
            boolean success = importService.loadDictionary(file);
            if(success) count++;
        }
        final String message = count + "/" + files.length + " dictionary files imported successfully.";
        return message;
    }
}
