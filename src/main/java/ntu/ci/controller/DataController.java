package ntu.ci.controller;

import ntu.ci.model.GraphQueryResults;
import ntu.ci.model.Node;

import ntu.ci.service.CacheService;
import ntu.ci.service.GraphService;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

/**
 * Created by dgee on 9/1/17.
 */

@RestController
@RequestMapping("/api")
public class DataController {

    private final Logger logger = LoggerFactory.getLogger(DataController.class);

    @Autowired
    private GraphService graphService;

    @Autowired
    private CacheService cacheService;

    @RequestMapping("/test")
    public String test() {
        logger.debug("Test method invoked! Generating mock response back to client .. ");
        graphService.testConnection();
        return "{\"id\":1,\"content\":\"Some Test Data\"}";
    }

    @RequestMapping("/query/{searchLabel}/{count}/{limit}")
    public GraphQueryResults query_labelForRootNode(
            @PathVariable String searchLabel,
            @PathVariable int count,
            @PathVariable int limit) {

        logger.info("/query invoked with params: query={}, count={}, limit={}", searchLabel, count, limit);
        return graphService.findTopWordsConnected(searchLabel, count, limit).orElse(new GraphQueryResults());
    }

    @RequestMapping("/add")
    public String add_newLabelToSelectedNodes(
            @RequestParam("type") String type,
            @RequestParam("label") String label,
            @RequestParam("rids") String [] rids) {

        logger.info("/add invoked with params: class={}, label={}, rids={}", type, label, String.join(",", rids));
        Optional<String> createdRid = graphService.createAbstractRelations(type, label, rids);

        if(createdRid.isPresent()) {
            return JSONObject.quote( createdRid.get() );
        }
        else {
            logger.error("Failed to capture RID of newly aggregated node [{}].", label);
            return JSONObject.quote("");
        }
    }

    @RequestMapping("/expand")
    public GraphQueryResults expand_fromSelectedNodes(
            @RequestParam("rids") String [] rids,
            @RequestParam("relation") String edgeType,
            @RequestParam("count") int count,
            @RequestParam("limit") int limit) {

        logger.info("/expand invoked with params: edge type={}, count={}, each limit={}, rids={}", edgeType, count, limit, String.join(",", rids));
        return graphService.expandNodes(rids, edgeType, count, limit).orElse(new GraphQueryResults());
    }

    @RequestMapping("/schema/{choice}")
    public Set<String> getSchema(
            @PathVariable String choice) {

        logger.info("/schema invoked with params: choice={}", choice);

        switch(choice) {
            case "topic": return cacheService.getTopicsAvailable();
            case "class": return cacheService.getVertexTypesAvailable();
            case "relations": return cacheService.getEdgeTypesAvailable();
            default: return new HashSet<String>(0);
        }
    }
}
