package ntu.ci.model;

/**
 * Created by dgee on 9/1/17.
 */
public class Edge {

    private Object id;
    private String label;
    private Object fromId;
    private Object toId;

    public Edge(Object id, String label, Object fromId, Object toId) {
        this.id = id;
        this.label = label;
        this.fromId = fromId;
        this.toId = toId;
    }

    public Object getId() {
        return id;
    }

    public void setId(Object id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Object getFromId() {
        return fromId;
    }

    public void setFromId(Object fromId) {
        this.fromId = fromId;
    }

    public Object getToId() {
        return toId;
    }

    public void setToId(Object toId) {
        this.toId = toId;
    }
}
