package ntu.ci.model;

import java.util.Objects;
import java.util.Optional;

/**
 * Created by spirals on 22/1/17.
 */
public class RawData {
    private final static int numOfCols = 11;

    private String section;
    private int doc;
    private int sentence;

    private String relation;
    private String node1;
    private int pos1;

    private String node2;
    private int pos2;

    private String id1;
    private String id2;

    private String topic;

    public RawData() {}

    public static Optional<RawData> fromCsv(String line) {
        String [] arr = line.split(",");

        if(arr.length != numOfCols) {
            return Optional.empty();
        }
        else {
            String _doc = arr[1];
            String _sentence = arr[2];

            String _pos1 = arr[5];
            String _pos2 = arr[7];

            boolean validFormat = parseAllInteger(_doc, _sentence, _pos1, _pos2);

            if(validFormat) {
                RawData rd = new RawData();
                rd.setSection(arr[0]);
                rd.setDoc(Integer.parseInt(_doc));
                rd.setSentence(Integer.parseInt(_sentence));

                rd.setRelation(arr[3]);
                rd.setNode1(arr[4]);
                rd.setPos1(Integer.parseInt(_pos1));

                rd.setNode2(arr[6]);
                rd.setPos2(Integer.parseInt(_pos2));

                rd.setId1(arr[8]);
                rd.setId2(arr[9]);

                rd.setTopic(checkTopic(arr[10]));

                return Optional.of(rd);
            }
            else {
                return Optional.empty();
            }
        }
    }

    private static boolean parseAllInteger(String ... args) {
        boolean valid = true;

        for(String value: args) {
            try {
                Integer.parseInt(value);
            }
            catch(NumberFormatException ex) {
                valid = false;
            }
        }
        return valid;
    }

    private static String checkTopic(final String topic) {
        return topic.length() == 0
                ? "NA"
                : topic.toLowerCase();
    }

    public Node getNode1Object() {
        Node node = new Node(this.id1);
        node.addProperty("pos", this.pos1);
        node.addProperty("word", this.node1);
        node.addProperty("node_id", this.id1);
        addCommonProperties(node);
        return node;
    }

    public Node getNode2Object() {
        Node node = new Node(this.id2);
        node.addProperty("pos", this.pos2);
        node.addProperty("word", this.node2);
        node.addProperty("node_id", this.id2);
        addCommonProperties(node);
        return node;
    }

    private void addCommonProperties(Node node) {
        node.addProperty("doc", this.doc);
        node.addProperty("sentence", this.sentence);
        node.addProperty("section", this.section);
        node.addProperty("topic", this.topic);
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public int getDoc() {
        return doc;
    }

    public void setDoc(int doc) {
        this.doc = doc;
    }

    public int getSentence() {
        return sentence;
    }

    public void setSentence(int sentence) {
        this.sentence = sentence;
    }

    public String getRelation() {
        return relation;
    }

    public void setRelation(String relation) {
        this.relation = relation;
    }

    public String getNode1() {
        return node1;
    }

    public void setNode1(String node1) {
        this.node1 = node1;
    }

    public int getPos1() {
        return pos1;
    }

    public void setPos1(int pos1) {
        this.pos1 = pos1;
    }

    public String getNode2() {
        return node2;
    }

    public void setNode2(String node2) {
        this.node2 = node2;
    }

    public int getPos2() {
        return pos2;
    }

    public void setPos2(int pos2) {
        this.pos2 = pos2;
    }

    public String getId1() {
        return id1;
    }

    public void setId1(String id1) {
        this.id1 = id1;
    }

    public String getId2() {
        return id2;
    }

    public void setId2(String id2) {
        this.id2 = id2;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    @Override
    public boolean equals(Object o) {

        if (o == this) return true;
        if (!(o instanceof RawData)) {
            return false;
        }
        RawData rd = (RawData) o;
        return doc == rd.doc &&
                Objects.equals(section, rd.section) &&
                Objects.equals(sentence, rd.sentence);
    }

    @Override
    public int hashCode() {
        return Objects.hash(doc, section, sentence);
    }

    @Override
    public String toString() {
        return new StringBuffer("doc=").append(this.doc)
                .append(", sentence=").append(this.sentence)
                .append(", relation=").append(this.relation)
                .append(", node1=").append(this.node1)
                .append(", node2=").append(this.node2)
                .append(", id1=").append(this.id1)
                .append(", id2=").append(this.id2)
                .append(", topic=").append(this.topic)
                .toString();
    }
}
