package ntu.ci.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Created by dgee on 23/2/17.
 */
public class GraphQueryResults {

    private List<SimpleRelation> relations;
    private List<DataNode> nodes;

    public GraphQueryResults() {
        init();
    }

    public void addRelation(SimpleRelation relation) {
        this.relations.add(relation);
    }

    public void buildNodes() {
        this.nodes = relations.stream()
                .map(relation -> this.map(relation))
                .flatMap(datanodes -> datanodes.stream())
                .filter(this.distinctByKey(dn -> dn.getId()))
                .collect(Collectors.toList());
    }

    private void init() {
        this.relations = new ArrayList<>(0);
        this.nodes = new ArrayList<>(0);
    }

    public List<SimpleRelation> getRelations() {
        return relations;
    }

    public List<DataNode> getNodes() {
        return nodes;
    }

    public <T> Predicate<T> distinctByKey(Function<? super T, Object> keyExtractor)
    {
        Map<Object, Boolean> map = new ConcurrentHashMap<>();
        return t -> map.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
    }

    private List<DataNode> map(SimpleRelation relation) {
        DataNode source = new DataNode(relation.getSourceId().toString(), relation.getSourceWord(), relation.getSourceClass());
        DataNode dest = new DataNode(relation.getDestId().toString(), relation.getDestWord(), relation.getDestClass());
        return Arrays.asList(source, dest);
    }

    private class DataNode {
        private String id;
        private String word;
        private String type;

        public DataNode() {
        }

        public DataNode(String id, String word, String type) {
            this.id = id;
            this.word = word;
            this.type = type;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getWord() {
            return word;
        }

        public void setWord(String word) {
            this.word = word;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        @Override
        public String toString() {
            return "DataNode [id=" + id + ", word=" + word + ", type=" + type + "]";
        }
    }
}
