package ntu.ci.model;

import com.tinkerpop.blueprints.Vertex;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by dgee on 9/1/17.
 */
public class Node {

    private Object id;
    private Map<String, Object> properties;

    public Node(Object id) {
        this.id = id;
        init();
    }

    private void init() {
        this.properties = new HashMap<String, Object>(0);
    }

    public Object getId() {
        return id;
    }

    public void setId(Object id) {
        this.id = id;
    }

    public Map<String, Object> getProperties() {
        return properties;
    }

    public void setProperties(Map<String, Object> properties) {
        this.properties = properties;
    }

    public void addProperty(final String key, final Object value) {
        this.properties.put(key, value);
    }

    public static Node build(Vertex v) {
        final Node node = new Node(v.getId());

        Map<String, Object> props = new HashMap<String, Object>(0);
        for(String key: v.getPropertyKeys()) {
            final Object val = v.getProperty(key);
            props.put(key, val);
        }

        node.setProperties(props);
        return node;
    }
}
