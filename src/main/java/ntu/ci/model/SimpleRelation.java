package ntu.ci.model;

import com.orientechnologies.orient.core.id.ORID;
import com.orientechnologies.orient.core.record.impl.ODocument;

/**
 * Created by dgee on 20/2/17.
 */
public class SimpleRelation {

    private String sourceWord;
    private Object sourceId;
    private String sourceClass;

    private String destWord;
    private Object destId;
    private String destClass;

    private String edge;
    private int count;

    public SimpleRelation() {
    }

    public SimpleRelation source(final String word, final Object id, final String _class) {
        this.sourceWord = word;
        this.sourceId = id;
        this.sourceClass = _class;
        return this;
    }

    public SimpleRelation dest(final String word, final Object id, final String _class) {
        this.destWord = word;
        this.destId = id;
        this.destClass = _class;
        return this;
    }

    public SimpleRelation edge(final String label) {
        this.edge = label;
        return this;
    }

    public SimpleRelation count(final int count) {
        this.count = count;
        return this;
    }

    public SimpleRelation(ODocument doc) {
        final String _sourceWord = doc.field("sourceWord");
        final String _sourceId = ((ODocument)doc.field("sourceId")).getIdentity().toString();
        final String _sourceClass = doc.field("sourceClass");

        final String _destWord = doc.field("destWord");
        final String _destId = ((ODocument)doc.field("destId")).getIdentity().toString();
        final String _destClass = doc.field("destClass");

        final String _edge = doc.field("edgeLabel");
        final int _count = doc.field("count");

        this.source(_sourceWord, _sourceId, _sourceClass)
            .dest(_destWord, _destId, _destClass)
            .edge(_edge)
            .count(_count);
    }

    public String getSourceWord() {
        return sourceWord;
    }

    public void setSourceWord(String sourceWord) {
        this.sourceWord = sourceWord;
    }

    public Object getSourceId() {
        return sourceId;
    }

    public void setSourceId(Object sourceId) {
        this.sourceId = sourceId;
    }

    public String getDestWord() {
        return destWord;
    }

    public void setDestWord(String destWord) {
        this.destWord = destWord;
    }

    public Object getDestId() {
        return destId;
    }

    public void setDestId(Object destId) {
        this.destId = destId;
    }

    public String getEdge() {
        return edge;
    }

    public void setEdge(String edge) {
        this.edge = edge;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getSourceClass() {
        return sourceClass;
    }

    public void setSourceClass(String sourceClass) {
        this.sourceClass = sourceClass;
    }

    public String getDestClass() {
        return destClass;
    }

    public void setDestClass(String destClass) {
        this.destClass = destClass;
    }
}
