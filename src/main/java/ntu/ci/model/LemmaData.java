package ntu.ci.model;

import java.util.Optional;

/**
 * Created by dgee on 6/2/17.
 */
public class LemmaData {
    private final static int numOfCols = 2;

    private String rawWord;
    private String lemmaWord;

    public LemmaData() {
    }

    public LemmaData(String rawWord, String lemmaWord) {
        this.rawWord = rawWord;
        this.lemmaWord = lemmaWord;
    }

    public static Optional<LemmaData> fromCsv(String line) {
        String [] arr = line.split(",");

        if(arr.length != numOfCols) {
            return Optional.empty();
        }
        else {
            final String _raw = arr[0];
            final String _lemma = arr[1];
            return Optional.of( new LemmaData(_raw, _lemma) );
        }
    }

    public String getRawWord() {
        return rawWord;
    }

    public String getLemmaWord() {
        return lemmaWord;
    }
}
