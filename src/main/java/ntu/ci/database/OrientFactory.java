package ntu.ci.database;

import com.orientechnologies.orient.client.remote.OServerAdmin;
import com.orientechnologies.orient.core.metadata.schema.OType;
import com.tinkerpop.blueprints.Parameter;
import com.tinkerpop.blueprints.Vertex;
import com.tinkerpop.blueprints.impls.orient.OrientGraph;
import com.tinkerpop.blueprints.impls.orient.OrientGraphFactory;
import com.tinkerpop.blueprints.impls.orient.OrientVertexType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Repository;

import javax.annotation.PreDestroy;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by dgee on 10/1/17.
 */

@Repository
public class OrientFactory {

    private final Logger logger = LoggerFactory.getLogger(OrientFactory.class);

    @Value("${orientdb.host}") private String host;
    @Value("${orientdb.database}") private String dbName;
    @Value("${orientdb.user}") private String username;
    @Value("${orientdb.password}") private String password;
    @Value("${orientdb.recreate}") private String recreate;
    private final String dbMode = "plocal";
    private final String dbType = "graph";
    private boolean ready = false;

    // Components
    private OrientGraphFactory factory;

    public OrientFactory() {}

    @Async
    public void start() {
        validate();
        init();
        checkSchema();
        ready = true;
        logger.info("Orient Graph DB is started.");
    }

    private void validate() {
        final String dbUrl = "remote:".concat(host);
        try {
            logger.debug("Connecting to OrientDB @ [{}] ..", dbUrl);
            OServerAdmin serverAdmin = new OServerAdmin(dbUrl);
            serverAdmin.connect(username, password);

            if(serverAdmin.existsDatabase(dbName, dbMode)) {
                logger.info("Database {} already exists.", dbName);

                if(needRecreate()) {
                    logger.info("Configured to re-create database on properties file.");
                    serverAdmin.dropDatabase(dbName, dbMode);

                    logger.info("Dropped database {}.", dbName);
                    try{TimeUnit.SECONDS.sleep(1);}catch(InterruptedException ex){}

                    serverAdmin.createDatabase(dbName, dbType, dbMode);
                    logger.info("Re-created database {}.", dbName);
                }
            }
            else {
                logger.info("Creating database {} of type {} in storage mode {}.", dbName, dbType, dbMode);
                serverAdmin.createDatabase(dbName, dbType, dbMode);
            }

            serverAdmin.close();
            logger.info("Closed admin connection to database.");
        }
        catch(IOException ex) {
            logger.error("Failed to connect/execute admin commands.", ex);
        }
    }

    private boolean needRecreate() {
        if(recreate.equals("yes")) {
            return true;
        }
        return false;
    }

    private void init() {
        final String dbPath = "remote:".concat(host).concat("/").concat(dbName);
        logger.debug("Initializing Orient Graph on {}.", dbPath);

        factory = new OrientGraphFactory(dbPath, username, password).setupPool(1, 50);
    }

    private void checkSchema() {
        OrientGraph graph = getFactory().getTx();
        graph.setUseLightweightEdges(false);

        List<String> types = Arrays.asList("Text", "Lemma");

        OrientVertexType vertexText = graph.getVertexType( types.get(0) );
        if(vertexText == null) {
            types.forEach(type -> {
                logger.info("Creating vertex type [{}].", type);
                graph.createVertexType(type);
            });

            vertexText = graph.getVertexType( types.get(0) );
            logger.info("Create property [word] for indexing.");
            vertexText.createProperty("word", OType.STRING);

            OrientVertexType vertexLemma = graph.getVertexType( types.get(1) );
            logger.info("Create property [lemmatization] for indexing.");
            vertexLemma.createProperty("lemmatization", OType.STRING);

            logger.info("Creating edge [lemmaOf].");
            graph.createEdgeType("lemmaOf");

            logger.info("Creating index for [word].");
            graph.createKeyIndex("word", Vertex.class, new Parameter("class", "Text"));

            logger.info("Creating index for [lemmatization].");
            graph.createKeyIndex("lemmatization", Vertex.class, new Parameter("class", "Lemma"));
        }
        else {
            logger.info("Vertex schemas {} already exists.", Arrays.toString(types.toArray()));
        }
    }

    public void shutdown() {
        logger.info("Shutting down Orient Graph Factory.");
        if(factory != null) {
            factory.close();
        }
    }

    public void drop() {
        final String dbUrl = "remote:".concat(host);

        try {
            logger.warn("You are dropping [{}] database. This will delete all tables under it.", dbName);
            OServerAdmin serverAdmin = new OServerAdmin(dbUrl);
            serverAdmin.connect(username, password);

            serverAdmin.dropDatabase(dbName, dbMode);

            serverAdmin.close();
            logger.info("Closed admin connection to database.");
        }
        catch(IOException ex) {
            logger.error("Failed to execute admin drop database command.", ex);
        }
    }

    public String getHost() {
        return host;
    }

    public String getDbName() {
        return dbName;
    }

    public OrientGraphFactory getFactory() {
        return factory;
    }

    public boolean isReady() {
        return ready;
    }

    @PreDestroy
    public void destroy() {
        shutdown();
    }
}
