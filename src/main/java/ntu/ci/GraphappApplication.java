package ntu.ci;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.AsyncConfigurerSupport;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.Executor;

@SpringBootApplication
@EnableScheduling
@EnableAsync
public class GraphappApplication extends AsyncConfigurerSupport {

	public static void main(String[] args) {
		SpringApplication.run(GraphappApplication.class, args);
	}

	@Override
	public Executor getAsyncExecutor() {
		final ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
		executor.setCorePoolSize(256);
		executor.setMaxPoolSize(256);
		executor.setQueueCapacity(40);
		executor.setThreadNamePrefix("AsyncCIGraph-");
		executor.initialize();

		return executor;
	}
}
