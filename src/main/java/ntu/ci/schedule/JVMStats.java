package ntu.ci.schedule;

import org.springframework.stereotype.Component;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;

/**
 * Created by dgee on 11/1/17.
 */

@Component
public class JVMStats {
    private static final Logger logger = LoggerFactory.getLogger(JVMStats.class);

    @Scheduled(initialDelay = 4000, fixedRate = 60000)
    private void printMemoryUsage() {
        printThreadsStatsitics();
        printMemoryStatistics();
    }

    private void printMemoryStatistics() {
        int mb = 1024*1024;
        StringBuffer buf = new StringBuffer();

        //Getting the runtime reference from system
        Runtime runtime = Runtime.getRuntime();

        //Print used memory
        buf.append("Used Memory:"
                + (runtime.totalMemory() - runtime.freeMemory()) / mb);

        //Print free memory
        buf.append(", Free Memory:"
                + runtime.freeMemory() / mb);

        //Print total available memory
        buf.append(", Total Memory:" + runtime.totalMemory() / mb);

        //Print Maximum available memory
        buf.append(", Max Memory:" + runtime.maxMemory() / mb);

        logger.debug(buf.toString());
    }

    private void printThreadsStatsitics() {
        Set<Thread> threads = Thread.getAllStackTraces().keySet();
        long nbRunning = threads.stream().filter(t -> t.getState() == Thread.State.RUNNABLE).count();

        logger.debug("Running {} out of {} threads.", nbRunning, threads.size());
    }
}
