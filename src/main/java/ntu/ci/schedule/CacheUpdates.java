package ntu.ci.schedule;

import ntu.ci.service.CacheService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * Created by spirals on 4/3/17.
 */

@Component
public class CacheUpdates {

    private static final Logger logger = LoggerFactory.getLogger(CacheUpdates.class);

    @Autowired
    private CacheService cacheService;

    @Scheduled(initialDelay = 60_000, fixedRate = 30_000)
    private void updateCache() {
        logger.info("Cache Update Service checking ..");

        if(cacheService.hasUpdates()) {
            logger.info("Updating Cache ..");
            cacheService.init();
            cacheService.updated();
        }
        else {
            logger.info("No new updates.");
        }
    }
}
