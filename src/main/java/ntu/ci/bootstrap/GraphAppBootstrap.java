package ntu.ci.bootstrap;

import ntu.ci.database.OrientFactory;
import ntu.ci.service.CacheService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

/**
 * Created by dgee on 11/1/17.
 */

@Component
public class GraphAppBootstrap implements ApplicationRunner {

    private static final Logger logger = LoggerFactory.getLogger(GraphAppBootstrap.class);

    @Autowired
    private OrientFactory orientFactory;

    @Autowired
    private CacheService cacheService;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        logger.info("Starting Orient DB ..");
        orientFactory.start();

        try {
            while(!orientFactory.isReady()) {
                TimeUnit.SECONDS.sleep(1);
            }

            logger.info("Initializing cache service ..");
            cacheService.init();

        }
        catch(InterruptedException ex) {}
    }
}
