package ntu.ci.service;

import com.orientechnologies.orient.core.db.document.ODatabaseDocumentTx;
import com.orientechnologies.orient.core.id.ORID;
import com.orientechnologies.orient.core.record.impl.ODocument;
import com.orientechnologies.orient.core.sql.OCommandSQL;
import com.orientechnologies.orient.core.sql.query.OSQLSynchQuery;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;
import com.tinkerpop.blueprints.impls.orient.*;
import ntu.ci.database.OrientFactory;
import ntu.ci.model.GraphQueryResults;
import ntu.ci.model.Node;
import ntu.ci.model.SimpleRelation;
import org.javatuples.Pair;
import org.javatuples.Quartet;
import org.javatuples.Triplet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * Created by dgee on 10/1/17.
 */

@Service
public class GraphService {

    private final Logger logger = LoggerFactory.getLogger(GraphService.class);

    @Autowired
    private OrientFactory orientFactory;

    @Autowired
    private CacheService cacheService;

    public void testConnection() {
        logger.debug("Graph Service invoked! {}", orientFactory.getHost());
    }

    /** ========================  **/
    /** ==========Query=========  **/
    /** ========================  **/

    public Optional<GraphQueryResults> findTopWordsConnected(final String queryWord, final int count, final int limit) {
        OrientGraphNoTx graph = orientFactory.getFactory().getNoTx();
        ODatabaseDocumentTx database = orientFactory.getFactory().getDatabase();

        Optional<GraphQueryResults> result = Optional.empty();

        final String osql = new StringBuilder()
                .append("select count, inV().lemmatization as sourceWord, inV()['@rid'] as sourceId, inV()['@class'] as sourceClass, ")
                .append("outV().lemmatization as destWord, outV()['@rid'] as destId, outV()['@class'] as destClass, @class as edgeLabel from ")
                .append("(Select expand(bothE()) from ?) ")
                .append("where count > ? order by count desc limit ?")
                .toString();

        try {
            Iterator<Vertex> iter = graph.getVertices("Lemma.lemmatization", queryWord.toLowerCase()).iterator();

            if(iter.hasNext()) {
                OrientVertex vertex = graph.getVertex(iter.next());
                final ORID rid = vertex.getIdentity();

                logger.debug("Found vertex [{}] to start as root node.", rid);

                List<ODocument> results = database
                        .command(new OSQLSynchQuery<ODocument>(osql)).execute(rid, count, limit);

                final List<SimpleRelation> relations = results.stream()
                        .map(relation -> new SimpleRelation(relation))
                        .collect(Collectors.toList());

                logger.debug("Found {} relations for query word [{}].", relations.size(), queryWord);

                if(relations.size() > 0) {
                    GraphQueryResults graphQueryResults = new GraphQueryResults();

                    relations.forEach(r -> graphQueryResults.addRelation(r));
                    graphQueryResults.buildNodes();

                    result = Optional.of(graphQueryResults);

                    logger.debug("Built graph query results with {} distinct nodes.", graphQueryResults.getNodes().size());
                }
            }

            else {
                logger.info("Couldn't find any node with word [{}].", queryWord);
            }
        }
        catch(Exception ex) {
            logger.error("Failed to execute search for root node [{}] with count > {}.", queryWord, count, ex);
        }
        finally {
            graph.shutdown();
            database.close();
            return result;
        }
    }

    public Optional<GraphQueryResults> expandNodes(final String [] rids, final String edgeType, final int count, final int eachLimit) {
        OrientGraphNoTx graph = orientFactory.getFactory().getNoTx();
        ODatabaseDocumentTx database = orientFactory.getFactory().getDatabase();

        Optional<GraphQueryResults> result = Optional.empty();
        final List<SimpleRelation> relations = new ArrayList<>(0);
        boolean hasEdgeQuery = false;

        final StringBuilder sb = new StringBuilder()
                .append("select count, inV().lemmatization as sourceWord, inV()['@rid'] as sourceId, inV()['@class'] as sourceClass, ")
                .append("outV().lemmatization as destWord, outV()['@rid'] as destId, outV()['@class'] as destClass, @class as edgeLabel from ");

        if(edgeType.equals("any")) {
            sb.append("(Select expand(bothE()) from ?) ");
        }
        else {
            sb.append("(Select expand(bothE(?)) from ?) ");
            hasEdgeQuery = true;
        }

        final String osql = sb.append("where count > ? order by count desc limit ?").toString();
        logger.debug("Expand Node OSql Query: {}", osql);

        try {
            GraphQueryResults graphQueryResults = new GraphQueryResults();

            for(final String rid: rids) {
                logger.debug("Getting vertex with rid={}.", rid);

                OrientVertex v = graph.getVertex(rid);
                final ORID orid = v.getIdentity();

                final List<ODocument> results;

                if(hasEdgeQuery) {
                    results = database
                            .command(new OSQLSynchQuery<ODocument>(osql)).execute(edgeType, orid, count, eachLimit);
                }
                else {
                    results = database
                            .command(new OSQLSynchQuery<ODocument>(osql)).execute(orid, count, eachLimit);
                }

                if(results != null) {
                    logger.debug("Adding graph query results for rid={}, edge={}, count={}, limit={}.", rid, edgeType, count, eachLimit);

                    results.stream()
                            .map(relation -> new SimpleRelation(relation))
                            .forEach(r -> graphQueryResults.addRelation(r));
                }
            }

            graphQueryResults.buildNodes();

            result = Optional.of(graphQueryResults);
        }
        catch(Exception ex) {
            logger.error("Failed to expand node for selected rids=[{}] with --{}-- relation on count > {}, each limit to {} results.",
                    String.join(",", rids), edgeType, count, eachLimit, ex);
        }
        finally {
            graph.shutdown();
            database.close();
            return result;
        }
    }

    /** ==========================  **/
    /** ==========Actions=========  **/
    /** ==========================  **/

    public Optional<String> createAbstractRelations(final String abstractClass, final String label, final String [] rids) {
        OrientGraph graph = orientFactory.getFactory().getTx();
        Optional<String> result = Optional.empty();

        final long startTime = System.currentTimeMillis();

        final List<Triplet<String, Integer, Object>> inRelationsWithCount = new ArrayList<>(0);
        final List<Triplet<String, Integer, Object>> outRelationsWithCount = new ArrayList<>(0);

        try {
            final List<Vertex> vertexList = Arrays.stream(rids)
                    .map(rid -> graph.getVertex(rid))
                    .filter(v -> v != null)
                    .collect(Collectors.toList());

            if(vertexList.size() == rids.length) {

                vertexList.stream()
                        .forEach(v -> {
                            Iterable<Edge> inEdges = v.getEdges(Direction.IN);
                            Iterable<Edge> outEdges = v.getEdges(Direction.OUT);

                            StreamSupport.stream(inEdges.spliterator(), false)
                                    .filter(edge -> !edge.getLabel().equals("lemmaOf"))
                                    .forEach(edge -> {
                                        Vertex sourceV = edge.getVertex(Direction.OUT);
                                        final int edgeCount = edge.getProperty("count");

                                        Triplet<String, Integer, Object> trip = Triplet.with(edge.getLabel(), edgeCount, sourceV.getId());
                                        logger.debug("Triplet added: {}", trip.toString());

                                        inRelationsWithCount.add(trip);
                                    });

                            StreamSupport.stream(outEdges.spliterator(), false)
                                    .filter(edge -> !edge.getLabel().equals("lemmaOf"))
                                    .forEach(edge -> {
                                        Vertex destV = edge.getVertex(Direction.IN);
                                        final int edgeCount = edge.getProperty("count");

                                        Triplet<String, Integer, Object> trip = Triplet.with(edge.getLabel(), edgeCount, destV.getId());
                                        logger.debug("Triplet added: {}", trip.toString());

                                        outRelationsWithCount.add(trip);
                                    });
                        });

                final Map<Pair<String, Object>, Integer> inRelationWithSum =
                        inRelationsWithCount.stream()
                                .collect(Collectors.groupingBy(trip -> Pair.with(trip.getValue0(), trip.getValue2()),
                                        Collectors.summingInt(Triplet::getValue1))
                                );

                final Map<Pair<String, Object>, Integer> outRelationWithSum =
                        outRelationsWithCount.stream()
                                .collect(Collectors.groupingBy(trip -> Pair.with(trip.getValue0(), trip.getValue2()),
                                        Collectors.summingInt(Triplet::getValue1))
                                );

                logger.debug("Aggregating {} IN relations into {} groups. Aggregating {} OUT relations in {} groups.",
                        inRelationsWithCount.size(), inRelationWithSum.size(), outRelationsWithCount.size(), outRelationWithSum.size());

                OrientVertexType vertexType = graph.getVertexType(abstractClass);
                if(vertexType == null) {
                    logger.debug("Creating vertex type [{}] now.", abstractClass);
                    graph.createVertexType(abstractClass);
                }

                logger.debug("Creating {} label of class type {} now.", label, abstractClass);
                final Vertex abstractVertex = graph.addVertex("class:".concat(abstractClass), "lemmatization", label);

                logger.debug("Commit the abstract vertex first.");
                graph.commit();
                TimeUnit.MILLISECONDS.sleep(150);

                result = Optional.ofNullable(graph.getVertex(abstractVertex).getId().toString() );

                inRelationWithSum.forEach( (k, v) -> {
                    final String edge = k.getValue0();
                    final Object sourceId = k.getValue1();
                    final int count = v;

                    final Vertex targetV = graph.getVertex(abstractVertex);
                    Vertex sourceV = graph.getVertex(sourceId);

                    if(sourceV != null) {
                        OrientEdge oEdge = graph.addEdge(null, sourceV, targetV, edge);
                        oEdge.setProperty("count", count);
                    }
                });

                outRelationWithSum.forEach( (k, v) -> {
                    final String edge = k.getValue0();
                    final Object sourceId = k.getValue1();
                    final int count = v;

                    final Vertex sourceV = graph.getVertex(abstractVertex);
                    Vertex targetV = graph.getVertex(sourceId);

                    if(sourceV != null) {
                        OrientEdge oEdge = graph.addEdge(null, sourceV, targetV, edge);
                        oEdge.setProperty("count", count);
                    }
                });

                logger.info("Aggregation of edges for label [{}] done!", label);

                graph.commit();

            }
            else {
                logger.warn("Failed to find all vertices from these rids={}. Will not aggregate them.", String.join(",", rids));
            }
        }
        catch(Exception ex) {
            result = Optional.empty();
            logger.error("Failed to create abstract relations. Class={}, Label={}, Selected RIDs=[{}].",
                    abstractClass, label, String.join(",", rids), ex);

            graph.rollback();
        }
        finally {
            graph.shutdown();

            final long diff = System.currentTimeMillis() - startTime;
            logger.info("Took {}ms to create abstract relation for [{}].", diff, label);
            cacheService.needsUpdate();

            return result;
        }
    }

    public boolean saveDocument(final Map<String, Node> map, final List<Triplet<String, String, String>> relations) {
        final Map<String, ODocument> tmp = new HashMap<>(0);
        OrientGraph graph = orientFactory.getFactory().getTx();
        boolean success = true;

        try {
            map.keySet().forEach(nodeId -> {
                Node node = map.get(nodeId);

                OrientVertex vx = graph.addVertex("class:Text");
                vx.setProperties(node.getProperties());

                tmp.put(nodeId, vx.getRecord());
            });

            logger.debug("Done import {} nodes.", map.size());

            relations.forEach(rs -> {
                final OrientVertex v1 = graph.getVertex(tmp.get(rs.getValue0()));
                final OrientVertex v2 = graph.getVertex(tmp.get(rs.getValue2()));
                final String edgeLabel = rs.getValue1();

                graph.addEdge(null, v1, v2, edgeLabel);
            });

            logger.debug("Done import {} relations.", relations.size());

            graph.commit();
        }
        catch(Exception ex) {
            logger.error("Failed to process graph nodes.", ex);
            graph.rollback();
            success = false;
        }
        finally {
            graph.shutdown();
            return success;
        }
    }

    public boolean clearLemmaLayer() {
        OrientGraph graph = orientFactory.getFactory().getTx();
        boolean success = true;

        try {
            final String osql = "DELETE VERTEX Lemma BATCH 5000";
            final int modified = graph.command(new OCommandSQL(osql)).execute();

            logger.debug("Result of Clearing Lemma Layer: {}.", modified);
            graph.commit();
        }
        catch(Exception ex) {
            logger.error("Failed to clean Lemma layer.", ex);
            graph.rollback();
            success = false;
        }
        finally {
            graph.shutdown();
            return success;
        }
    }

    @Async
    public CompletableFuture<Boolean> saveLemma(final Map<String, List<String>> lemmaDictionary ) {
        OrientGraph graph = orientFactory.getFactory().getTx();
        boolean success = true;

        final long startTime = System.currentTimeMillis();

        try {
            lemmaDictionary.forEach( (lemma, words) -> {
                logger.debug("Lemma={}, Words={}.", lemma, Arrays.toString(words.toArray()));

                Vertex lemmaNode = graph.addVertex("class:Lemma", "lemmatization", lemma);

                words.forEach(word -> {
                    Iterable<Vertex> vertices = graph.getVertices("Text.word", word);
                    vertices.forEach(v -> {
                        graph.addEdge(null, graph.getVertex(lemmaNode), v, "lemmaOf");
                    });
                });
            });

            graph.commit();
        }
        catch(Exception ex) {
            logger.error("Failed to save data into Lemma layer.", ex);
            graph.rollback();
            success = false;
        }
        finally {
            graph.shutdown();

            final long endTime = System.currentTimeMillis();

            logger.debug("Took {}ms to process {} Lemma words. Success: {}", (endTime-startTime), lemmaDictionary.size(), success);

            return CompletableFuture.completedFuture(success);
        }
    }

    public Map<Triplet<String, String, String>, Integer> aggregateAllEdges(final String ... edgeFilters) {
        OrientGraphNoTx graph = orientFactory.getFactory().getNoTx();
        final String filters = String.join(":::", edgeFilters);
        Map<Triplet<String, String, String>, Integer> relationWithSum = new HashMap<>(0);

        final long startTime = System.currentTimeMillis();

        try {
            Iterable<Edge> edges = graph.getEdges();
            final List<Quartet<String, String, String, Integer>> relationWithCount = new ArrayList<>(0);
            final String propertyToMap = "word";

            StreamSupport.stream(edges.spliterator(), false)
                    .filter(edge -> !filters.contains(edge.getLabel()))
                    .forEach(edge ->  {
                        Vertex outV = edge.getVertex(Direction.OUT);
                        Vertex inV = edge.getVertex(Direction.IN);

                        Quartet<String, String, String, Integer> quad =
                                Quartet.with(outV.getProperty(propertyToMap), edge.getLabel(), inV.getProperty(propertyToMap), 1);
                        relationWithCount.add(quad);
                    });

            relationWithSum = relationWithCount.stream()
                    .collect(Collectors.groupingBy(quad -> Triplet.with(quad.getValue0(), quad.getValue1(), quad.getValue2()),
                        Collectors.summingInt(Quartet::getValue3))
                    );
        }
        finally {
            graph.shutdown();

            final long endTime = System.currentTimeMillis();
            logger.debug("Took {}ms to aggregate all {} edges.", (endTime-startTime), relationWithSum.size());
        }

        return relationWithSum;
    }

    public boolean linkLemma(final Map<Triplet<String, String, String>, Integer> aggEdgesWithRelations) {
        OrientGraph graph = orientFactory.getFactory().getTx();
        boolean success = true;

        final long startTime = System.currentTimeMillis();

        try {
            aggEdgesWithRelations.forEach( (k, v) -> {
                final String outWord = k.getValue0();
                final String edge = k.getValue1();
                final String inWord = k.getValue2();
                final int count = v;

                Vertex outText = graph.getVertices("Text.word", outWord).iterator().next();
                Vertex inText = graph.getVertices("Text.word", inWord).iterator().next();

                if(outText != null && inText != null) {
                    Iterator<Vertex> outLemma = outText.getVertices(Direction.IN, "lemmaOf").iterator();
                    Iterator<Vertex> inLemma = inText.getVertices(Direction.IN, "lemmaOf").iterator();

                    if(outLemma.hasNext() && inLemma.hasNext()) {
                        logger.debug("Aggregate {} --{}--> {} with count {}.", outWord, edge, inWord, count);
                        OrientEdge oEdge = graph.addEdge(null, outLemma.next(), inLemma.next(), edge);
                        oEdge.setProperty("count", count);
                    }
                }
            });

            graph.commit();
        }
        catch(Exception ex) {
            logger.error("Failed to aggregate edges on Lemma layer.", ex);
            graph.rollback();
            success = false;
        }
        finally {
            graph.shutdown();

            final long endTime = System.currentTimeMillis();

            logger.debug("Took {}ms to process {} Lemma words. Success: {}", (endTime-startTime), aggEdgesWithRelations.size(), success);

            cacheService.needsUpdate();

            return success;
        }
    }
}
