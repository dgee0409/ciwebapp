package ntu.ci.service;

import com.orientechnologies.orient.core.db.document.ODatabaseDocumentTx;
import com.orientechnologies.orient.core.record.impl.ODocument;
import com.orientechnologies.orient.core.sql.query.OSQLSynchQuery;
import ntu.ci.database.OrientFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by dgee on 28/2/17.
 */

@Service
public class CacheService {

    private final Logger logger = LoggerFactory.getLogger(CacheService.class);

    private Set<String> topics;
    private Set<String> types;
    private Set<String> edges;

    private boolean updates = false;

    @Autowired
    private OrientFactory orientFactory;

    @Async
    public void init() {
        ODatabaseDocumentTx database = orientFactory.getFactory().getDatabase();

        topics = new HashSet<String>(0);
        types = new HashSet<String>(0);
        edges = new HashSet<String>(0);

        final String osql_topic = "SELECT distinct(topic) as uniq from Text";
        final String osql_vertex = "SELECT distinct(@class) as uniq from V";
        final String osql_edge = "SELECT distinct(@class) as uniq from E";

        try {
            final long startTime = System.currentTimeMillis();

            List<ODocument> results = database.command(new OSQLSynchQuery<ODocument>(osql_topic)).execute();
            processUnique(results, topics);
            results.clear();

            results = database.command(new OSQLSynchQuery<ODocument>(osql_vertex)).execute();
            processUnique(results, types);
            results.clear();

            results = database.command(new OSQLSynchQuery<ODocument>(osql_edge)).execute();
            processUnique(results, edges);
            results.clear();

            final long endTime = System.currentTimeMillis();
            final long diff = endTime - startTime;

            logger.info("Cache Service reporting in {}ms: Topics={}, Vertex types={}, Edge types={}.",
                    diff, topics.size(), types.size(), edges.size());

        }
        catch(Exception ex) {
            logger.error("Failed to init cache service.", ex);
        }
        finally {
            database.close();
        }
    }

    private void processUnique(List<ODocument> documents, Set<String> cache) {
        documents.forEach(doc -> {
            final String uniqValue = doc.field("uniq");
            cache.add(uniqValue);
        });
    }

    public Set<String> getTopicsAvailable() {
        return topics;
    }

    public Set<String> getVertexTypesAvailable() {
        return types;
    }

    public Set<String> getEdgeTypesAvailable() {
        return edges;
    }

    public boolean hasUpdates() {
        return updates;
    }

    public void needsUpdate() {
        this.updates = true;
    }

    public void updated() {
        this.updates = false;
    }

}
