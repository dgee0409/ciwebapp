package ntu.ci.service;

import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import ntu.ci.model.LemmaData;
import ntu.ci.model.Node;
import ntu.ci.model.RawData;
import org.javatuples.Triplet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Created by dgee on 11/1/17.
 */

@Service
public class ImportService {

    private final Logger logger = LoggerFactory.getLogger(ImportService.class);
    private final String symbolRegex = "[^A-Za-z0-9_-]";

    @Autowired
    private GraphService graphService;

    public boolean processData(MultipartFile file) {
        boolean success = true;
        logger.debug("Processing file [{}] ..", file.getOriginalFilename());

        File tempFile = null;
        final String tempName = "tmp-file";

        try {
            tempFile = File.createTempFile(tempName, ".tmp");
            file.transferTo(tempFile);
            logger.debug("Created temp file {}.", tempFile.getAbsolutePath());

            if(saveRawIntoGraph(tempFile)) {
                logger.info("Successfully imported [{}] data into graph.", file.getOriginalFilename());
            }
            else {
                success = false;
                logger.warn("Failed to import [{}] data into graph.", file.getOriginalFilename());
            }
        }
        catch (IOException ex) {
            logger.error("Failed to process file [{}].", file.getOriginalFilename(), ex);
            success = false;
        }
        finally {
            if(tempFile != null) {
                boolean deleted = tempFile.delete();
                logger.info("Deleting tmp file {}: {}", tempFile.getAbsolutePath(), success == true ? "ok" : "fail");
            }
        }

        return success;
    }

    private boolean saveRawIntoGraph(final File inputFile) throws IOException {
        FileInputStream inputStream = null;
        Scanner sc = null;

        int count = 0;
        int failCount = 0;

        try {
            inputStream = new FileInputStream(inputFile.getAbsolutePath());
            sc = new Scanner(inputStream, "UTF-8");

            Map<String, Node> map = new HashMap<String, Node>(0);
            List<Triplet<String, String, String>> relations = new ArrayList<>(0);
            RawData prevRd = null;
            int lineNo = 1;

            Pattern pattern = Pattern.compile(symbolRegex);

            String header = sc.nextLine();
            logger.debug("CSV Header: {}", header);

            while (sc.hasNextLine()) {
                String line = sc.nextLine();
                lineNo++;

                Optional<RawData> rawData = RawData.fromCsv(line);

                if (rawData.isPresent()) {
                    RawData rd = rawData.get();
                    count++;
                    checkInvalidCharacters(rd, pattern, lineNo);

                    // if doc and sentence is different
                    if (prevRd != null && !prevRd.equals(rd)) {

                        // Memory consumption consideration
                        if(map.size() >= 50) {
                            logger.info("Flush {} nodes into database first.", map.size());

                            boolean imported = graphService.saveDocument(map, relations);
                            if(imported) {
                                logger.info("Graph DB committed.");
                            }
                            else {
                                logger.info("Failed to import into Graph DB.");
                            }

                            map.clear();
                            relations.clear();
                            logger.debug("Map and relations size have been reset: {}, {}.", map.size(), relations.size());
                        }
                    }

                    Node n1 = rd.getNode1Object();
                    Node n2 = rd.getNode2Object();

                    if (!map.containsKey(rd.getId1())) {
                        map.put(rd.getId1(), n1);
                    }
                    if (!map.containsKey(rd.getId2())) {
                        map.put(rd.getId2(), n2);
                    }

                    Triplet<String, String, String> rs = Triplet.with(rd.getId1(), rd.getRelation(), rd.getId2());
                    relations.add(rs);

                    prevRd = rd;
                } else {
                    logger.warn("Failed to parse into raw data. Line: {}.", line);
                    failCount++;
                }
            }

            if(map.size() > 0 && relations.size() > 0) {
                boolean imported = graphService.saveDocument(map, relations);

                if(imported) {
                    logger.info("Saved {} lines. {} lines failed to import.", count, failCount);
                }
                else {
                    logger.warn("Failed to save {} nodes and {} relations.", map.size(), relations.size());
                }
            }
            else {
                logger.info("Nothing left to import.");
            }

            map.clear();
            relations.clear();

            // note that Scanner suppresses exceptions
            if (sc.ioException() != null) {
                throw sc.ioException();
            }
        } finally {
            if (inputStream != null) {
                inputStream.close();
            }
            if (sc != null) {
                sc.close();
            }
        }
        return true;
    }

    @Async
    private void checkInvalidCharacters(final RawData rawData, final Pattern p, final int lineNo) {
        Matcher n1Word = p.matcher(rawData.getNode1());
        Matcher n2Word = p.matcher(rawData.getNode2());

        Matcher n1Id = p.matcher(rawData.getId1());
        Matcher nd2Id = p.matcher(rawData.getId2());

        if(n1Word.find() || n2Word.find() || n1Id.find() || nd2Id.find()) {
            logger.warn("Line {} contains special characters. Raw Data was: {}", lineNo, rawData.toString());
        }
    }

    public boolean loadDictionary(MultipartFile file) {
        boolean success = true;
        logger.debug("Processing file [{}] ..", file.getOriginalFilename());

        File tempFile = null;
        final String tempName = "tmp-dictionary";

        final long start = System.currentTimeMillis();

        try {
            tempFile = File.createTempFile(tempName, ".tmp");
            file.transferTo(tempFile);
            logger.debug("Created temp file {}.", tempFile.getAbsolutePath());

            if(saveLemmaIntoGraph(tempFile)) {
                logger.info("Successfully imported [{}] dictionary into graph.", file.getOriginalFilename());
            }
            else {
                success = false;
                logger.warn("Failed to import [{}] dictionary into graph.", file.getOriginalFilename());
            }
        }
        catch (IOException ex) {
            logger.error("Failed to process file [{}].", file.getOriginalFilename(), ex);
            success = false;
        }
        finally {
            if(tempFile != null) {
                boolean deleted = tempFile.delete();
                logger.info("Deleting tmp file {}: {}", tempFile.getAbsolutePath(), success == true ? "ok" : "fail");
            }
        }

        final long end = System.currentTimeMillis();
        final long diff = end - start;

        final long minutes = diff / 1000 / 60;
        if(minutes > 0) {
            logger.info("Build dictionary took about {} minutes.", minutes);
        }
        else {
            logger.info("Build dictionary took about {} seconds.", diff/1000);
        }

        return success;
    }

    private boolean saveLemmaIntoGraph(final File inputFile) throws IOException {
        if(!graphService.clearLemmaLayer()) {
            logger.warn("Will not process dictionary file as we failed to clear Lemma layer.");
            return false;
        }

        boolean success = true;
        FileInputStream inputStream = null;
        Scanner sc = null;

        try {
            inputStream = new FileInputStream(inputFile.getAbsolutePath());
            sc = new Scanner(inputStream, "UTF-8");

            String header = sc.nextLine();
            logger.debug("CSV Header: {}", header);

            final List<LemmaData> lemmaList = new ArrayList<>(0);
            int lineNo = 0;
            int imported = 0;

            while (sc.hasNextLine()) {
                String line = sc.nextLine();
                lineNo++;

                Optional<LemmaData> ld = LemmaData.fromCsv(line);

                if(ld.isPresent()) {
                    lemmaList.add(ld.get());
                    imported++;
                }
                else {
                    logger.warn("Failed to parse in dictionary. Line Num {}: {}.", lineNo, line);
                }
            }

            if(lemmaList.size() > 0) {
                logger.info("Total number of Lemma lines to process: {}", lemmaList.size());

                final List<Map<String, List<String>>> lemmaInChunks = partitionIntoChunks(lemmaList);

                final List<CompletableFuture<Boolean>> futures = lemmaInChunks.stream()
                        .map(lemmaWords -> graphService.saveLemma(lemmaWords))
                        .collect(Collectors.toList());

                final CompletableFuture<List<Boolean>> allFutures = CompletableFuture.allOf(futures.toArray(new CompletableFuture[futures.size()]))
                        .thenApply(v -> futures.stream()
                            .map(future -> future.join())
                                .collect(Collectors.toList())
                        );

                final CompletableFuture<Boolean> anyFailedFuture = allFutures.thenApply(completedFutures ->
                        completedFutures.stream()
                        .filter(result -> result != null)
                        .map(result -> result.booleanValue())
                        .anyMatch(result -> result == false));


                final boolean anyFailed = anyFailedFuture.get().booleanValue();
                if(anyFailed) success = false;

                if(success) {
                    logger.info("{} lemma words saved.", lemmaList.size());

                    logger.info("Processing aggregation of all edges on Lemma Layer now ..");

                    Map<Triplet<String, String, String>, Integer> aggEdgesMap = graphService.aggregateAllEdges("lemmaOf");
                    boolean aggSuccess = graphService.linkLemma(aggEdgesMap);

                    if(!aggSuccess) {
                        success = false;
                        logger.warn("Failed to aggregate all edges into lemma layer.");
                    }
                }
            }
            else {
                logger.info("No Lemma dictionary detected.");
            }

        }
        catch(Exception ex) {
            logger.error("Error while importing lemma dictionary.", ex);
            success = false;
        }

        finally {
            if (inputStream != null) {
                inputStream.close();
            }
            if (sc != null) {
                sc.close();
            }
        }

        return success;
    }

    private List<Map<String, List<String>>> partitionIntoChunks(final List<LemmaData> lemmaList) {
        final int numOfPartitions = 10;
        final int numOfWordsPerPartition = lemmaList.size() / (numOfPartitions);

        // lemma_word -> list of (raw words)
        final Map<String, List<String>> lemmaDictionary = lemmaList.stream()
                .collect(Collectors.groupingBy(LemmaData::getLemmaWord,
                        Collectors.mapping(LemmaData::getRawWord, Collectors.toList())));

        logger.info("{} Lemma words to be chunk in partitions of {} each.", lemmaDictionary.size(), numOfWordsPerPartition);

        // partition number of lemma words
        Iterable<List<String>> subsets = Iterables.partition(lemmaDictionary.keySet(), numOfWordsPerPartition);

        // sub maps declaration
        final List<Map<String, List<String>>> chunks = new ArrayList<>(0);

        // build the chunks of submaps
        int partitionNo = 0;
        final Iterator<List<String>> iterator = subsets.iterator();
        while(iterator.hasNext()) {
            List<String> subLemmaWords = iterator.next();

            //Build sub maps
            final Map<String, List<String>> submap = subLemmaWords.stream()
                    .collect(Collectors.toMap(
                            word -> word,
                            word -> lemmaDictionary.get(word)));

            if(submap.size() > 0) {
                logger.info("Chunking partition {} of {} words.", partitionNo++, submap.size());
                chunks.add(submap);
            }
        }

        return chunks;
    }
}
