// =====================
// Query
// =====================
app.controller('queryDialogController', function($scope, $http, $uibModalInstance, ctrlInstance, cy, growl) {

    $scope.submit = function() {
        var api = './api/query/' + $scope.subject + '/' + $scope.count + '/' + $scope.limit;

        $http.get(api)
            .then(function(response) {

                if(response.data.nodes.length == 0) {
                    growl.warning('Your query returns no results.');
                }
                else {
                    populateGraph(cy, ctrlInstance, response.data);
                    growl.success('Received query results!');
                }
                callbackFn();
            })

        function callbackFn(){
            $uibModalInstance.dismiss('save');
        }
    };

    $scope.submitDisabled = function() {
        if (typeof $scope.subject == 'undefined' ||
            typeof $scope.count == 'undefined' ||
            typeof $scope.limit == 'undefined') {

            return true;
        }
        return $scope.subject.length == 0 || $scope.count.length == 0 || $scope.limit.length == 0;
    };

    $scope.close = function() {
		$uibModalInstance.dismiss('close');
	};

    function populateGraph(cy, ctrlInstance, data) {
        console.log('DEBUG: Response object from Query.');
        console.log(data);

        // Init cytoscape
        cy = cytoscape({
            container: $('#cy'),
            elements: [],
            style: [],
            layout: []
         });

        // Get all the edge labels
        var uniqueEdges = buildEdgesStyle(data.relations);

        // Build the style first
        var selectors = [];
        var nodeStyles = getNodeStyle();
        var nodeSelectedStyle = getNodeSelectedStyle();

        selectors.push(nodeStyles);
        selectors.push(nodeSelectedStyle);
        addEdgeStyleIntoSelectors(selectors, uniqueEdges);

        // Built cytoscape graph

        // Add the nodes first
        for(var idx in data.nodes) {
            var node = data.nodes[idx];

            cy.add( {
                data: {
                    id: node.id,
                    word: node.word,
                    class: node.type,
                    selected: false
                }
            });
        }

        // Build the edges
        for(var idx in data.relations) {
            var relation = data.relations[idx];
            var edgeId = 'edge#' + idx;
            var edgeLabelWithCount = relation.edge + '(' + relation.count + ')';

            cy.add( {
                data: {
                    id: edgeId,
                    label: edgeLabelWithCount,
                    group: relation.edge,
                    source: relation.sourceId,
                    target: relation.destId
                }
            });
        }

        cy.style().fromJson(selectors).update();

        cy.boxSelectionEnabled( true );

        cy.layout({
            name: 'circle',
            }
        );

        // Set to control instance so that another controller can use
        ctrlInstance.cy = cy;

    }

    function buildEdgesStyle(relations) {
        var uniqueEdges = relations.map(function(elem, index, relations) {
                return elem.edge;
            })
            .filter(function(elem, index, relations) {
                return relations.indexOf(elem) == index;
        });

        uniqueEdges.push('aggregate');

        return uniqueEdges;
    }

    function addEdgeStyleIntoSelectors(selectors, uniqueEdges) {
        for(var idx in uniqueEdges) {
            var edgeName = uniqueEdges[idx];
            var color = getRandomColor();
            var style = {
                "selector": "edge[group='" + edgeName + "']",
                "style": {
                    "label": "data(label)",
                    "line-color": color,
                    "curve-style": "bezier",
                    "target-arrow-shape": "triangle",
                    "target-arrow-color": color,
                }
            };
            selectors.push(style);
        };

        var autoRotateStyle = {
            selector: '.autorotate',
            style: {
              'edge-text-rotation': 'autorotate'
            }
        };

        selectors.push(autoRotateStyle);
    }

    function getNodeStyle() {
        var style = {
            "selector": "node",
            "style": {
                "content": "data(word)",
                "text-valign": "center",
                "color": "white",
                "text-outline-width": 2,
                "background-color": "#999",
                "text-outline-color": "#999"
            }
        };

        return style;
    }

    function getNodeSelectedStyle() {
        var style = {
            "selector": "node:selected",
            "style": {
                "border-width": "6px",
                "border-color": "#AAD8FF",
                "border-opacity": "0.5",
                "background-color": "#77828C",
                "text-outline-color": "#77828C"
            }
        };

        return style;
    }

    function getRandomColor() {
        var letters = '02468BCDEF';
        var color = '#';
        for (var i = 0; i < 6; i++ ) {
            color += letters[Math.floor(Math.random() * 10)];
        }
        return color;
    }
});