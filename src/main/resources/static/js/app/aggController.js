// =====================
// Aggregate
// =====================
app.controller('aggregateDialogController', function($scope, $http, $uibModalInstance, cy, selectedNodes, growl) {

    $scope.selectedNodes = selectedNodes;

    $scope.submit = function() {
        var selectedRids = $scope.selectedNodes.map(function(elem) {
            return elem.id;
        })

        var params = {
            type: $scope.nodeType,
            label: $scope.nodeLabel,
            rids: selectedRids
        };

        $http({
            method: 'GET',
            url: '/api/add',
            params: params
            }
        )
        .then(function(response) {
            console.log("/api/add response")

            var rid = response.data;
            if(rid.length > 1) {
                addAggregatedEdge(cy, rid, params);
                growl.success('Node has been aggregated.');
            }
            else {
                growl.fail('Fail to aggregate nodes.');
            }

            callbackFn();
        });

        function callbackFn(){
            $uibModalInstance.dismiss('save');
        }
    };

    $scope.submitDisabled = function() {
        if($scope.selectedNodes.length == 0 || $scope.selectedNodes == 'undefined') {
            return true;
        }

        if (typeof $scope.nodeLabel == 'undefined' ||
            typeof $scope.nodeType == 'undefined') {

            return true;
        }
        return $scope.nodeLabel.length == 0 || $scope.nodeType.length == 0;
    };

    $scope.close = function() {
		$uibModalInstance.dismiss('close');
	};

	function addAggregatedEdge(cy, rid, params) {
	    // create new node
	    cy.add( {
            data: {
                id: rid,
                word: params.label,
                class: params.type,
                selected: false
            }
        });

        // create all edges
        var idx = 0;

        for(let selectedRid of params.rids) {
            var edgeId = 'aggregatedEdge#' + idx;
            idx++;

            cy.add( {
                data: {
                    id: edgeId,
                    label: 'aggregatedTo',
                    group: 'aggregate',
                    source: selectedRid,
                    target: rid
                }
            });

            cy.layout({
                name: 'cose',
                }
            );
        }
	}
});