// =====================
// Expand
// =====================
app.controller('expandDialogController', function($scope, $http, $uibModalInstance, cy, selectedNodes, growl) {

    $scope.selectedNodes = selectedNodes;

    $scope.init = function() {
    $http.get('./api/schema/relations')
        .then(function(response) {
            console.log(response);
            $scope.relations = Array.from(response.data);
        });
    }

    $scope.submit = function() {
        var selectedRids = $scope.selectedNodes.map(function(elem) {
            return elem.id;
        })

        var params = {
            rids: selectedRids,
            relation: $scope.relation,
            count: $scope.count,
            limit: $scope.limit
        };

        $http({
            method: 'GET',
            url: '/api/expand',
            params: params
            }
        )
        .then(function(response) {
            console.log("/api/expand response")

            if(response.data.nodes.length == 0) {
                growl.warning('Unable to find relations paired with selected nodes.');
            }
            else {
                expandNodeRelations(cy, response.data);
                growl.info('New relations have been expanded..');
            }

            callbackFn();
        });

        function callbackFn(){
            $uibModalInstance.dismiss('save');
        }
    };

    $scope.submitDisabled = function() {
        if($scope.selectedNodes.length == 0 || $scope.selectedNodes == 'undefined') {
            return true;
        }

        if (typeof $scope.relation == 'undefined' ||
            typeof $scope.count == 'undefined' ||
            typeof $scope.limit == 'undefined') {

            return true;
        }
        return $scope.relation.length == 0 ||
            $scope.count.length == 0 ||
            $scope.limit.length == 0;
    };

    $scope.close = function() {
		$uibModalInstance.dismiss('close');
	};

	$scope.init();

	function expandNodeRelations(cy, data) {
	    console.log('DEBUG: Response data from expand');
	    console.log(data);

        addNodes(data.nodes);
        addRelations(data.relations);

        var uniqueEdges = getUniqueEdges(data.relations);
        buildStyle(uniqueEdges, cy);

        cy.layout({
            name: 'concentric',
            concentric: function( node ){
              return node.degree();
            },
            levelWidth: function( nodes ){
              return 2;
            }
          }
        );
	}

	function addNodes(nodes) {
        for(var idx in nodes) {
            var node = nodes[idx];
            var cyNode = cy.getElementById(node.id);

            if(cyNode.length === 0) {
                cy.add( {
                    data: {
                        id: node.id,
                        word: node.word,
                        class: node.type,
                        selected: false
                    }
                });
            }
        }
	}

	function addRelations(relations) {
	    for(var idx in relations) {
            var relation = relations[idx];
            var uuid = createUUID();
            var edgeId = 'edge#' + uuid;
            var edgeLabelWithCount = relation.edge + '(' + relation.count + ')';

            cy.add( {
                data: {
                    id: edgeId,
                    label: edgeLabelWithCount,
                    group: relation.edge,
                    source: relation.sourceId,
                    target: relation.destId
                }
            });
        }
	}

	function getUniqueEdges(relations) {
	    var uniqueEdges = relations.map(function(elem, index, relations) {
                return elem.edge;
            })
            .filter(function(elem, index, relations) {
                return relations.indexOf(elem) == index;
        });

        uniqueEdges.push('aggregate');

        return uniqueEdges;
	}

	function buildStyle(uniqueEdges) {
	    for(var idx in uniqueEdges) {
            var edgeName = uniqueEdges[idx];
            var color = getRandomColor();

            cy.style()
                .selector("edge[group='" + edgeName + "']")
                .style({
                    "label": "data(label)",
                    "line-color": color,
                    "curve-style": "bezier",
                    "target-arrow-shape": "triangle",
                    "target-arrow-color": color,
                })
                .update();
        };
	}

	function getRandomColor() {
        var letters = '0479BDEF';
        var color = '#';
        for (var i = 0; i < 6; i++ ) {
            color += letters[Math.floor(Math.random() * 8)];
        }
        return color;
    }

    function createUUID() {
        // http://www.ietf.org/rfc/rfc4122.txt
        var s = [];
        var hexDigits = "0123456789abcdef";
        for (var i = 0; i < 12; i++) {
            s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
        }
        s[14] = "4";
        s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1);
        s[8] = s[13] = s[18] = s[23] = "-";

        var uuid = s.join("");
        return uuid;
    }
});