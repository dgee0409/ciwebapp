var app = angular.module('app', ['ui.bootstrap', 'angular-growl', 'bootstrap3-typeahead']);

app.config(['growlProvider', function(growlProvider) {
    growlProvider.globalPosition('bottom-right');
    growlProvider.globalTimeToLive(5000); // notification will disappear automatically after 5000ms (5 secs).
}]);

var counter = 0;

app.controller('appController', function($scope, $http, $uibModal, growl) {

    var cy = null;
    $scope.cy = cy;
    var ctrl = $scope;

    $scope.dataresult = 'nothing';

    $scope.files = [];
    $scope.dictionaries = [];

    $scope.doQuery = function() {
        $uibModal.open({
            templateUrl: './views/modal/queryDialog.html',
            controller: 'queryDialogController',
            size: 'lg',
            resolve: {
                ctrlInstance: function() {return ctrl; },
                cy: function() {return cy; }
            }
        });
    };

    $scope.clearGraph = function() {
        growl.warning('Graph elements has been removed!');
        $scope.cy.elements().remove();
    };

    $scope.aggregateNode = function() {
        var selectedNodes = getSelectedNodes();

        $uibModal.open({
            templateUrl: './views/modal/aggregateDialog.html',
            controller: 'aggregateDialogController',
            size: 'lg',
            resolve: {
                cy: function() {return $scope.cy; },
                selectedNodes: function() {return selectedNodes;}
            }
        });
    }

    $scope.expandNode = function() {
        var selectedNodes = getSelectedNodes();

        $uibModal.open({
            templateUrl: './views/modal/expandDialog.html',
            controller: 'expandDialogController',
            size: 'lg',
            resolve: {
                cy: function() {return $scope.cy; },
                selectedNodes: function() {return selectedNodes;}
            }
        });
    }

    $scope.addDisabled = function() {
        return $scope.cy == null;
    };

    // File Import
    $scope.$watch('files', function() {
        if ($scope.files.length > 0) {
            importDataFiles();
        }
    });

    // Dictionary Import
    $scope.$watch('dictionaries', function() {
            if ($scope.dictionaries.length > 0) {
                importDictionaryFiles();
            }
    });

    function getSelectedNodes() {
        var selectedArray = [];

        $scope.cy.nodes(':selected').forEach(function(item, index) {
            selectedArray.push(item);
        });

        var selectedNodes = [];

        for(var idx in selectedArray) {
            var node = selectedArray[idx];
            selectedNodes.push({
                id: node.id(),
                data: node.data()
            })
        }

        return selectedNodes;
    }

    // Import dataset files into system
    function importDataFiles() {
        var files = $scope.files;
        var formData = new FormData();
        var key = 'files';

        for(var i=0; i < files.length; i++) {
            formData.append(key, files[i]);
        }

        var importNotification = growl.info('Importing data files ..', {ttl: -1, disableCloseButton: true});

        $.ajax({
            url: './import',
            data: formData,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function(response) {
                importNotification.destroy();
                growl.success(response);

                $scope.files = [];
                angular.forEach(
                    angular.element("input[type='file']"),
                    function(inputElem) {
                        angular.element(inputElem).val(null);
                 });
            },
            error: function(response) {
                console.log(response);
                growl.error('Failed to upload files.');
            }
        });
    }

    // Import dictionary files into system
    function importDictionaryFiles() {
        var files = $scope.dictionaries;
        var formData = new FormData();
        var key = 'files';

        for(var i=0; i < files.length; i++) {
            formData.append(key, files[i]);
        }

        var importNotification = growl.info('Building dictionary. This might take a while ..', {ttl: -1, disableCloseButton: true});

        $.ajax({
            url: './dictionary',
            data: formData,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function(response) {
                importNotification.destroy();
                growl.success(response);

                $scope.dictionaries = [];
                angular.forEach(
                    angular.element("input[type='file']"),
                    function(inputElem) {
                        angular.element(inputElem).val(null);
                 });
            },
            error: function(response) {
                console.log(response);
                growl.error('Failed to build dictionary.');
            }
        });
    }
});

// =======================================
// File Import Configurations (don't edit)
// =======================================
app.directive('ngFileModel', ['$parse', function($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var model = $parse(attrs.ngFileModel);
            var isMultiple = attrs.multiple;
            var modelSetter = model.assign;

            element.bind('change', function() {
                var values = [];
                angular.forEach(element[0].files, function(item) {
                    values.push(item);
                });

                scope.$apply(function () {
                    if (isMultiple) {
                        modelSetter(scope, values);
                    }
                    else {
                        modelSetter(scope, values[0]);
                    }
                });
            });
        }
    };
}]);