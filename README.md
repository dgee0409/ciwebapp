### Project Overview

The project uses the MVC framework from Spring. It runs an embedded Tomcat instance internally.

The design of the current framework is such that hopefully it scales down to a one page application.   
From query, to processing, to visualizing.

There are basically **3 main tasks** now:

* Design of storing the processed data into graph db
* Backend implementation of storing the data into graph db
* Frontend visualizing of the graph

### IDE Platform

IntelliJ Community Edition:
https://www.jetbrains.com/idea/download/

You may download the one with the larger download file size, this comes with configured settings.

Otherwise if you wish, you can download the latter but which you need to download and setup your Java environment as well.

### Java Environment

JDK 1.8.0_111 (comes with bundled IntelliJ as well)    

Please do not use JRE versions.

### Javascript Versions

* Jquery: 2.2.4
* Bootstrap: 3.3.7
* Angular: 1.5.8
* Angular UI Bootstrap: 2.4.0
* Cytoscape: 2.7.13
* Angular Growl v2: 0.7.8

### Importing Project into IntelliJ

Go to `VCS` -> `Check out from version control` -> `Git`

Under Repository URL, put in the bitbucket https url.

Import as a **Maven Project**.

Once imported successfully, enable auto-import of maven if prompt.

After all dependencies are imported, you may build the project.

Once built, just run the GraphappApplication.

You should see this log which indicates is a success:
```
INFO  ntu.ci.GraphappApplication - Started GraphappApplication in 3.482 seconds (JVM running for 3.735)
```

### Cytoscape

Main Website: http://js.cytoscape.org/

Tutorial Guide: http://blog.js.cytoscape.org/

1. http://blog.js.cytoscape.org/2016/05/24/getting-started/
2. http://blog.js.cytoscape.org/2016/06/08/glycolysis/
3. http://blog.js.cytoscape.org/2016/07/04/social-network/

### Orient DB

Main Website: http://orientdb.com/orientdb/    
Download: http://orientdb.com/download/      
Documentation: http://orientdb.com/docs/last/index.html

##### Setup

1. Download the binary distribution based on your OS
2. Unzip the distribution
3. Go to the `unzip directory`/bin folder
4. ./server.sh

This will start the OrientDB server.
It will prompt you to enter a password for user 'root'.

For CI project wise, please set password to "password".

Once password is set, go to http://localhost:2480.
You should see the web UI for OrientDB.

Login as 'root' and your password.

### Development Guides

Under the `src/test/java` package, there are a few basic examples on how to use OrientDB API.

Most methods that we should start with are annotated/marked as TODO comments.

Cytoscape basic example is also available at `src/main/resources/static/js/app.js` and `controller.js`.

#### Reset Database on Application Start

Under `src/main/resources/application.properties`, find the property `orientdb.recreate`.    

Change the value to `yes`.   

Start the Application again, the database would be re-created.

### Release & Distribution

This assumes that orientdb is setup on your PC and running.

From project path:

1. Package the project into JAR
```
$ mvn clean package -DskipTests
```

2. CIWebapp.jar is produced under the `project path/target` folder.

3. Running the web
```
$ java -jar CIWebapp.jar
```
### Example Queries

* Get the top lemma words that has the most edges connected to it

`select *, out('lemmaOf').size() as size from Lemma order by size desc`

* Traverse out from a particular node, within 3 depth levels, and omitting information from 1st level

`SELECT FROM (TRAVERSE out("lemmaOf"), out() FROM #24:2559 WHILE $depth <= 3) WHERE $depth > 1`

You can include relations in the 2nd out() as well. Eg. out("amod").

* Find all raw words that are linked to the lemma word

`select distinct(word) from (traverse out("lemmaOf") from #21:1982)`

* Find top 20 lemma words connected via [amod] relation to a node with count more than 1

`
select count, inV().lemmatization, inV()['@rid'], inV()['@class'] as class1, outV().lemmatization, outV()['@rid'], @class from (
  Select expand(bothE()) from #24:2559
)
where count > 1
and inV()['@class'] = 'Lemma'
and @class = 'amod'
order by count desc
limit 20;
`